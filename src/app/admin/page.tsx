"use client";
export default function AdminPage() {
  const handleAdminLoginClick = async () => {
    type AdminLoginResponse = {
      success: string;
      authKey: string;
      validUntil: number;
    };
    console.log("Running admin login function handler...");
    const dataRaw = await fetch("/api/admin", {
      method: "POST",
      body: JSON.stringify({
        adminName:
          (document.getElementById("admin-name") as HTMLInputElement).value,
        adminPassword:
          (document.getElementById("admin-password") as HTMLInputElement).value,
      }),
    });
    const responseBody = await dataRaw.json() as AdminLoginResponse;
    if (responseBody.success) {
      location.assign(`/api/backend?auth=${responseBody.authKey}`);
    } else alert("ERROR[1001] Authentication failure");
  };
  return (
    <main>
      <p>Admin page</p>
      <fieldset>
        <legend>Login credentials</legend>
        <label htmlFor="admin-name">Admin name</label>
        <input id="admin-name"></input>
        <br />
        <label htmlFor="admin-password">Admin password</label>{" "}
        <input id="admin-password"></input>
        <br />
        <button
          className="btn btn-primary"
          onClick={() => {
            handleAdminLoginClick();
          }}
        >
          Submit
        </button>
      </fieldset>
    </main>
  );
}
